module.exports = {
  verbose: false,
  plugins: {
    local: {
      browsers: ['chrome']
    },
    sauce: {
      disabled: true
    },
    "istanbul": {
      dir: "./coverage",
      reporters: ["text-summary", "lcov"],
      include: [
        "px-highchart-gauge.html"
      ],
      exclude: [
        "/polymer/polymer.js",
        "/platform/platform.js"
      ]
    }
  },
  suites: [
    'test/px-highchart-gauge-test-fixture.html'
  ]
};
