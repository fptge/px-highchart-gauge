// This is the wrapper for custom tests, called upon web components ready state
function runCustomTests() {
  // Place any setup steps like variable declaration and initialization here

  // This is the placeholder suite to place custom tests in
  // Use testCase(options) for a more convenient setup of the test cases
  suite('Custom Automation Tests for px-highchart-gauge', function() {
    test('Check chart object', function(done){
      var el = Polymer.dom(document).querySelector('px-highchart-gauge');
      assert.equal(typeof el.chart, 'object');
      assert.notEqual(el.chart, null);
      done();
    });

    test('Update chart data', function(done){
      var el = Polymer.dom(document).querySelector('px-highchart-gauge');

      el.updatePoint(12);
      assert.equal(el.chart.series[0].points[0].y, 12);

      flush(function(){
        assert.equal(el.getPoint(), 12);
      });
      done();
    });
  });
};
